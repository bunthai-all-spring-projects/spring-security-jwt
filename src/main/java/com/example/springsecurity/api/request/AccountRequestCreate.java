package com.example.springsecurity.api.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

/**
 * Project spring-security
 * Author bunthai.den
 * Date 6/9/2022
 */
@Data
public class AccountRequestCreate {

    @Email(message = RequestConstantParameter.EMAIL_MESSAGE)
    private String email;

    @JsonProperty(required = true)
    @Size(max = RequestConstantParameter.MAX_LENGTH_NAME)
    private String name;

    @JsonProperty(required = true)
    @Size(max = RequestConstantParameter.MAX_LENGTH_PASSWORD)
    private String password;

}
