package com.example.springsecurity.api.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

/**
 * Project spring-security
 * Author bunthai.den
 * Date 6/10/2022
 */
@Data
public class AuthenticateRequest {

    @Email(message = RequestConstantParameter.EMAIL_MESSAGE)
    private String email;

    @JsonProperty(required = true)
    @Size(max = RequestConstantParameter.MAX_LENGTH_PASSWORD)
    private String password;
}
