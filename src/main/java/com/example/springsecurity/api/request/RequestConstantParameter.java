package com.example.springsecurity.api.request;

/**
 * Project spring-security
 * Author bunthai.den
 * Date 6/10/2022
 */
public class RequestConstantParameter {
    public static final int MAX_LENGTH_NAME = 100;
    public static final int MAX_LENGTH_PASSWORD = 100;
    public static final String EMAIL_MESSAGE = "invalid email format";
}
