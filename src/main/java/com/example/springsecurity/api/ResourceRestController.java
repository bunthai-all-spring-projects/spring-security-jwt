package com.example.springsecurity.api;

import com.example.springsecurity.domain.Principal;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * Project spring-security
 * Author bunthai.den
 * Date 6/9/2022
 */
@RestController
@RequestMapping("api/v1/resource")
public class ResourceRestController {

    @GetMapping("user")
    public ResponseEntity<Map<String, String>> data() {
        return ResponseEntity.ok(new HashMap<String, String>() {{
            put("data", "some user data..");
        }});
    }

    @GetMapping("admin")
    public ResponseEntity<Map<String, String>> dataAdmin(Principal _principal) {

        System.out.println("aaaaaaaaaaaaaaaaaaaaaa: " + _principal);

        return ResponseEntity.ok(new HashMap<String, String>() {{
            put("data", "some admin data..");
        }});
    }

}
