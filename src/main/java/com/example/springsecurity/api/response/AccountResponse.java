package com.example.springsecurity.api.response;

import com.example.springsecurity.domain.Account;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Value;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

/**
 * Project spring-security
 * Author bunthai.den
 * Date 6/9/2022
 */
@Value
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class AccountResponse {

    String email;
    String name;

    public static AccountResponse of(Account account) {
        return new AccountResponse(account.getEmail(), account.getName());
    }
}
