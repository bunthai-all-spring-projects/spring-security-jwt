package com.example.springsecurity.api.response;

import lombok.Value;

import java.sql.Timestamp;

/**
 * Project spring-security
 * Author bunthai.den
 * Date 6/10/2022
 */
@Value
public class AuthenticateResponse {
    String token;
    long expiredAt;

    public AuthenticateResponse(String token, long expiredAt) {
        this.token = token;
        this.expiredAt = expiredAt;
    }
}
