package com.example.springsecurity.api;

import com.example.springsecurity.api.request.AccountRequestCreate;
import com.example.springsecurity.api.request.AuthenticateRequest;
import com.example.springsecurity.api.response.AccountResponse;
import com.example.springsecurity.api.response.AuthenticateResponse;
import com.example.springsecurity.domain.Account;
import com.example.springsecurity.service.AccountService;
import com.example.springsecurity.utils.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.SecretKeySpec;
import javax.validation.Valid;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.NoSuchElementException;

/**
 * Project spring-security
 * Author bunthai.den
 * Date 6/9/2022
 */
@RestController
@CrossOrigin("*")
public class AccountRestController {

    @Autowired
    AccountService accountService;


    @PostMapping("api/v1/account/register")
    public ResponseEntity<AccountResponse> register(
            @Valid
            @RequestBody AccountRequestCreate requestBody
    ) {
        String email = requestBody.getEmail();
        String name = requestBody.getName();
        String password = requestBody.getPassword();

        Account account = accountService.register(email, name, password, null);
        return ResponseEntity.status(HttpStatus.OK).body(AccountResponse.of(account));
    }


}
