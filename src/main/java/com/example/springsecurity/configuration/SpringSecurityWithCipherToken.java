package com.example.springsecurity.configuration;

import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

//
//import com.example.springsecurity.api.request.AuthenticateRequest;
//import com.example.springsecurity.api.response.AuthenticateResponse;
//import com.example.springsecurity.utils.Utility;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Lazy;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.ResponseEntity;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.builders.WebSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.config.http.SessionCreationPolicy;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
//import org.springframework.stereotype.Component;
//import org.springframework.web.bind.annotation.CrossOrigin;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.filter.OncePerRequestFilter;
//import org.springframework.web.servlet.HandlerExceptionResolver;
//
//import javax.crypto.Cipher;
//import javax.servlet.FilterChain;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.validation.Valid;
//import java.io.IOException;
//import java.sql.Date;
//import java.time.Instant;
//import java.util.ArrayList;
//import java.util.Base64;
//import java.util.NoSuchElementException;
//
///**
// * Project spring-security
// * Author bunthai.den
// * Date 6/9/2022
// */
//
//@EnableWebSecurity
//@RestController
//@CrossOrigin("*")
public class SpringSecurityWithCipherToken extends WebSecurityConfigurerAdapter {
//
//    @Autowired
//    @Lazy
//    UserDetailsService userDetailsService;
//    @Autowired
//    @Lazy
//    AuthenticationTokenFilter authenticationTokenFilter;
//
//    @PostMapping("api/v1/account/auth")
//    public ResponseEntity<AuthenticateResponse> auth(
//        @Valid
//        @RequestBody AuthenticateRequest requestBody
//    ) throws Exception {
//        String email = requestBody.getEmail();
//        String password = requestBody.getPassword();
//        UsernamePasswordAuthenticationToken credential = UsernamePasswordAuthenticationToken.authenticated(email, password, new ArrayList<>());
//        // check authenticated
//        Authentication authentication = authenticationManager().authenticate(credential);
//        if (authentication.isAuthenticated()) {
//            long timestamp = Date.from(Instant.now()).getTime();
//            String token = Base64.getEncoder().encodeToString(Utility.cipherProcess(Cipher.ENCRYPT_MODE, (email + ";" + timestamp).getBytes()));
//            AuthenticateResponse authenticateResponse = new AuthenticateResponse(token, timestamp);
//            return ResponseEntity.ok(authenticateResponse);
//        }
//        throw new NoSuchElementException("M404, not found");
//    }
//
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.userDetailsService(userDetailsService);
//    }
//
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//
//        http
//            .csrf().disable()
//            .authorizeHttpRequests().antMatchers("/api/v1/account/auth", "/api/v1/account/register").permitAll()
//            .anyRequest().authenticated()
//            .and()
//            .addFilterBefore(authenticationTokenFilter, UsernamePasswordAuthenticationFilter.class)
//            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
//        ;
//    }
//
//
//    @Override
//    public void configure(WebSecurity web) throws Exception {
//        web.ignoring().antMatchers(
//            "/h2-console/**"
//        );
//    }
//
//
//    @Component
//    class AuthenticationTokenFilter extends OncePerRequestFilter {
//
//        @Autowired
//        @Qualifier("handlerExceptionResolver")
//        private HandlerExceptionResolver resolver;
//
//        @Override
//        protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
//
//            try {
//                String authorization = request.getHeader(HttpHeaders.AUTHORIZATION);
//
//                if (authorization != null && authorization.toLowerCase().startsWith("bearer ")) {
//                    String token = authorization.replace("bearer ", "").replace("Bearer ", "");
//                    System.out.println(token);
//                    String info = new String(Utility.cipherProcess(Cipher.DECRYPT_MODE, Base64.getDecoder().decode(token)));
//                    System.out.println(info);
//                    String[] payload = info.split(";");
//                    String email = payload[0];
//                    long timestamp = Long.valueOf(payload[1]);
//                    long expired = Date.from(Instant.now().plusSeconds((-5 * 60))).getTime();
//
//                    System.out.println(timestamp + ";" + expired);
//                    if (timestamp <= expired)
//                        throw new IllegalArgumentException("400, token expired");
//
//                    UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(email, null, new ArrayList<>());
//                    // Authenticate user
//                    SecurityContextHolder.getContext().setAuthentication(authentication);
//
//                }
//
//                filterChain.doFilter(request, response);
//                System.out.println("---------------------");
//            } catch (Exception e) {
//                resolver.resolveException(request, response, null, e);
//            }
//        }
//    }
//
}
