package com.example.springsecurity.configuration.exception;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Map;

/**
 * Project spring-security
 * Author bunthai.den
 * Date 6/9/2022
 */
@RestControllerAdvice
public class ExceptionRestControllerAdvice {

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<ExceptionHandling> exceptionHandler(Exception ex) {

        String message = ex.getMessage();
        int code = HttpStatus.BAD_REQUEST.value();

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ExceptionHandling(message, code));
    }

    @AllArgsConstructor
    static
    class ExceptionHandling {
        public String message;
        public int code;
    }

}
