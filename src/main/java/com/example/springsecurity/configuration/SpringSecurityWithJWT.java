package com.example.springsecurity.configuration;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.example.springsecurity.api.request.AuthenticateRequest;
import com.example.springsecurity.api.response.AuthenticateResponse;
import com.example.springsecurity.utils.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Configuration
@RestController
@CrossOrigin("*")
public class SpringSecurityWithJWT implements WebMvcConfigurer {

    AuthenticationManager authenticationManager;
    @Autowired
    UserDetailsService userDetailsService;

    @Autowired
    @Lazy
    @Qualifier("handlerExceptionResolver")
    private HandlerExceptionResolver resolver;

    @Autowired
    @Lazy
    AuthenticationTokenFilter authenticationTokenFilter;

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
        resolvers.add(new UserPrincipalArgumentResolver());
    }

    @PostMapping("api/v1/account/auth")
    public ResponseEntity<AuthenticateResponse> auth(
        @Valid
        @RequestBody AuthenticateRequest requestBody
    ) throws Exception {
        String email = requestBody.getEmail();
        String password = requestBody.getPassword();
        UsernamePasswordAuthenticationToken credential = UsernamePasswordAuthenticationToken.authenticated(email, password, new ArrayList<>());
        Authentication authentication = authenticationManager.authenticate(credential);
        if (authentication.isAuthenticated()) {
            long timestamp = Date.from(Instant.now()).getTime();
            String role = authentication.getAuthorities().stream().findFirst().get().getAuthority();
            String token = Utility.jwtToken(email, role);
            AuthenticateResponse authenticateResponse = new AuthenticateResponse(token, timestamp);
            return ResponseEntity.ok(authenticateResponse);
        }
        throw new NoSuchElementException("M404, not found");
    }

    @Bean
    public SecurityFilterChain filterChain(HttpServletRequest _request, HttpServletResponse _response, HttpSecurity http) throws Exception {

        AuthenticationManagerBuilder authenticationManagerBuilder = http.getSharedObject(AuthenticationManagerBuilder.class);
        authenticationManagerBuilder.userDetailsService(userDetailsService);
        authenticationManager = authenticationManagerBuilder.build();

        http.csrf().disable().cors().disable().authorizeHttpRequests().antMatchers("/api/v1/account/register", "/api/v1/account/auth").permitAll()
            .antMatchers("/api/v1/resource/user").hasAnyAuthority("USER", "ADMIN")
            .antMatchers("/api/v1/resource/admin").hasAnyAuthority("ADMIN")
            .anyRequest().authenticated()
            .and()
            .authenticationManager(authenticationManager)
            .addFilterBefore(authenticationTokenFilter, UsernamePasswordAuthenticationFilter.class)
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
            return http.build();
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public WebSecurityCustomizer webSecurityCustomizer() {
        return (web) -> web.ignoring().antMatchers("/h2-console/**");
    }

    @Component
    class AuthenticationTokenFilter extends OncePerRequestFilter {

        @Override
        protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
            try {
                Optional<String> authorization = Optional.ofNullable(request.getHeader(HttpHeaders.AUTHORIZATION));
                authorization.ifPresent(_authorization -> {
                    if (_authorization.toLowerCase().startsWith("bearer")) {
                        String token = _authorization.replaceAll("Bearer ", "").replace("bearer ", "");

                        Algorithm algorithm = Algorithm.HMAC256(Utility.secret); //use more secure key
                        JWTVerifier verifier = JWT.require(algorithm)
                            .build(); //Reusable verifier instance
                        DecodedJWT jwt = verifier.verify(token);
                        String email = jwt.getClaims().get("email").asString();
                        String role = jwt.getClaims().get("role").asString();


                        Collection<GrantedAuthority> roles = new ArrayList<>();
                        roles.add(new SimpleGrantedAuthority(role));
                        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(email, null, roles);
                        SecurityContextHolder.getContext().setAuthentication(authenticationToken);


                    }
                });
                filterChain.doFilter(request, response);
            } catch (Exception e) {
                resolver.resolveException(request, response, null, e);
            }
        }
    }


}
