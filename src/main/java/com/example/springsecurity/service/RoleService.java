package com.example.springsecurity.service;

import com.example.springsecurity.domain.Role;
import com.example.springsecurity.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RoleService {

    @Autowired
    RoleRepository roleRepository;

    public Optional<Role> findRoleByLabel(String label) {
        Optional<Role> roleOpt = roleRepository.findRoleByLabel(label);
        if (roleOpt.isPresent()) {
            return roleOpt;
        }
        return Optional.empty();
    }

    public Role createRole(String _role) {
        Optional<Role> roleOpt = findRoleByLabel(_role);
        if (roleOpt.isPresent()) throw new IllegalArgumentException("role already exist");
        Role role = Role.Builder().label(_role).build();
        return roleRepository.save(role);
    }

}
