package com.example.springsecurity.service;

import com.example.springsecurity.domain.Account;
import com.example.springsecurity.domain.Role;
import com.example.springsecurity.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;
import java.util.Optional;

/**
 * Project spring-security
 * Author bunthai.den
 * Date 6/9/2022
 */
@Service
public class AccountService {

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    @Lazy
    BCryptPasswordEncoder bCryptPasswordEncoder;

    public Account register(String email, String name, String password, Role _role) {
        Account.AccountBuilder accountBuilder = Account.Builder().email(email).name(name).role(_role).password(bCryptPasswordEncoder.encode(password));
        Account newAccount = accountRepository.save(accountBuilder.build());
        return  newAccount;
    }

    public Account findAccountByEmail(String email) {
        Optional<Account> accountOpt = accountRepository.findAccountByEmail(email);
        Account account = accountOpt.orElseThrow(() -> new NoSuchElementException("404, account not found"));
        return  account;
    }



}
