package com.example.springsecurity.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.GeneralSecurityException;
import java.security.InvalidParameterException;
import java.sql.Date;
import java.time.Instant;
import java.util.HashMap;

/**
 * Project spring-security
 * Author bunthai.den
 * Date 6/10/2022
 */
public class Utility {

    //    @Value("${secretkey}")
    public static String secret = "mysecret";

    public static byte[] cipherProcess(int mode, byte[] data) {

        try {
            if (mode != Cipher.ENCRYPT_MODE && mode != Cipher.DECRYPT_MODE)
                throw new GeneralSecurityException();
            Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
            cipher.init(mode, secretKey());
            return cipher.doFinal(data);
        } catch (GeneralSecurityException e) {
            throw new InvalidParameterException("Invalid Token");
        }
    }

    public static SecretKey secretKey() {
        return new SecretKeySpec(secret.getBytes(), "DES");
    }

    public static String jwtToken(String email, String role) {
        Algorithm algorithm = Algorithm.HMAC256(secret);
        String token = JWT.create()
            .withPayload(new HashMap<String, String>() {{
                put("email", email);
                put("role", role);
            }})
            .withIssuer("Bunthai")
            .withExpiresAt(Date.from(Instant.now().plusSeconds(5)))
            .sign(algorithm);


        return token;
    }
}
