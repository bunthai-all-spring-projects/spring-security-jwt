package com.example.springsecurity;

import com.example.springsecurity.domain.Role;
import com.example.springsecurity.service.AccountService;
import com.example.springsecurity.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;

@SpringBootApplication
public class SpringSecurityApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(SpringSecurityApplication.class, args);
	}

    @Autowired
    AccountService accountService;

    @Autowired
    RoleService roleService;

    @Override
    public void run(String... args) throws Exception {

        Arrays.asList("USER", "ADMIN").forEach(_s -> roleService.createRole(_s));

        accountService.register("abc@gmail.com", "abc", "123", Role.Builder().id(1).build());
        accountService.register("admin@gmail.com", "admin", "123", Role.Builder().id(2).build());
    }
}
