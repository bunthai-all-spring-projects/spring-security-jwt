package com.example.springsecurity;

import com.example.springsecurity.utils.Utility;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import javax.crypto.Cipher;
import java.util.Base64;
import java.util.Objects;

@SpringBootTest
class SpringSecurityApplicationTests {

	@Test
	void contextLoads() {
        String inputData = "Hello world!";
        byte[] cipherByte = Utility.cipherProcess(Cipher.ENCRYPT_MODE, inputData.getBytes());
        byte[] data = Utility.cipherProcess(Cipher.DECRYPT_MODE, cipherByte);

        Assert.isTrue(Objects.equals(inputData, new String(data)), "correct");

	}

}
